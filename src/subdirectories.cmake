#*******************************************************************************
# @copyright Copyright (c)2017 John T Hill IV>; All rights reserved.
#
# @project  puzzlesolver
# @file     subdirectores.cmake
# @brief    Lists top level source directories to build packages and executables
#
# @author   John T Hill IV(jhill515@gmail.com
# @date     Jul 8, 2017
#
# @attention Microsoft Reference Source License (Ms-RSL)
# @attention See Ms-RSL.md or request from John T Hill IV for license details.
#*******************************************************************************

add_subdirectory(Tiles)
add_subdirectory(ThreadPool)
add_subdirectory(OutWriter)
add_subdirectory(Taskmaster)
add_subdirectory(Main)

#*******************************************************************************
# Copyright (c)2017 John T Hill iV; All rights reserved.
#
# Microsoft Reference Source License (Ms-RSL)
# See Ms-RSL.md or request from John T Hill IV for license details.
#*******************************************************************************