/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Tile.h
 * @brief    Structure to contain partial object edge-detection maps for edges
 *           of tiles. This is the minimum necessary data to represent a tile to
 *           assess puzzle solutions.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef TILE_H_
#define TILE_H_

//=== Includes  ================================================================

#include <cstdint>

#include "PuzzleConfig.h"


//=== Type Definitions =========================================================

/**
 * @class   Tile
 * @extends None
 * @brief   Represents an partial object edge map along puzzle tile edges
 */
class Tile
{
  public: /// @publicsection
    //--- Constructors/Destructor ----------------------------------------------

    /**
     * @name   Tile
     * @brief  Default construction of the Tile object.
     * @param  None
     * @return Tile : Constructed instance of Tile
     * @throws None
     */
    Tile();

    /**
     * @name   Tile
     * @brief  Construction of the Tile object with set edge length
     * @param  edgeLen (const std::uint32_t&) - length of one side of the image
     *         in pixels
     * @return Tile : Constructed instance of Tile with set edges
     * @throws None
     */
    Tile(const std::uint32_t& edgeLen);

    /**
     * @name   Tile
     * @brief  Deep-copy Construction of the Tile object
     * @param  original (const Tile&) - Original copy of the tile
     *         in pixels
     * @return Tile : Constructed instance of Tile
     * @throws None
     */
    Tile(const Tile& original);

    /**
     * @name   ~Tile
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~Tile();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   getEdge
     * @brief  Returns reference to the tile edge provided side and orientation
     * @param  orientation (const Orientation&) - orientation of the tile
     * @param  side (const TileSide&) - which side of the tile to retrieve
     *         relative to specified orientation
     * @return uint8_t* : Requested edge map with size edgeLength
     * @throws None
     */
    std::uint8_t*
    getEdge(const Orientation& orientation, const TileSide& side);

    /**
     * @name   getLength
     * @brief  Returns the length of the tile edge
     * @param  None
     * @return uint32_t : See edgeLength
     * @throws None
     */
    std::uint32_t
    getLength() { return edgeLength; }



  protected: /// @protectedsection
    //--- Protected Definitions ------------------------------------------------

    friend class Image;


    //--- Protected Members ----------------------------------------------------

    /**
     * @name   edge0
     * @brief  Partial object edge-detection map of the top edge of
     *         orientation 0; see Readme.md for details.
     * @units  N/A
     * @bounds N/A
     */
    std::uint8_t* edge0;

    /**
     * @name   edge01
     * @brief  Partial object edge-detection map of the right edge of
     *         orientation 0; see Readme.md for details.
     * @units  N/A
     * @bounds N/A
     */
    std::uint8_t* edge1;

    /**
     * @name   edge2
     * @brief  Partial object edge-detection map of the bottom edge of
     *         orientation 0; see Readme.md for details.
     * @units  N/A
     * @bounds N/A
     */
    std::uint8_t* edge2;

    /**
     * @name   edge3
     * @brief  Partial object edge-detection map of the left edge of
     *         orientation 0; see Readme.md for details.
     * @units  N/A
     * @bounds N/A
     */
    std::uint8_t* edge3;


  private: /// @privatesection
    //--- Private Members ------------------------------------------------------

    /**
     * @name   edgeLength
     * @brief  Length of the partial object edge-detection maps
     * @units  pixels
     * @bounds Must be greater than six, but unchecked
     */
    std::uint32_t edgeLength;

};

#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill iV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
