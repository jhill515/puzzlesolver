/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     PuzzleConfig.cpp
 * @brief    Implements puzzle configuration primatives.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "PuzzleConfig.h"


//=== Functions & Methods ======================================================

/**
 * @name   lessthanOrientation
 * @brief  Computes if lhs < rhs
 * @param  lhs (const Orientation&) - left side of less-than comparison
 * @param  rhs (const Orientation&) - right side of less-than comparison
 * @return bool : Returns TRUE if lhs < rhs
 * @throws None
 */
bool
lessthanOrientation(const Orientation& lhs, const Orientation& rhs)
{
  bool ret = false;
  switch(lhs)
  {
    case Orientation::eOrient0:
    {
      ret = (rhs != Orientation::eOrient0);
      break;
    }
    case Orientation::eOrient1:
    {
      ret = (rhs == Orientation::eOrient2 ||
             rhs == Orientation::eOrient3 );
      break;
    }
    case Orientation::eOrient2:
    {
      ret = (rhs == Orientation::eOrient3);
      break;
    }
    case Orientation::eOrient3:
    default: // Seems like a good idea for a catch-all?
    {
      ret = false;
      break;
    }
  }
  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   rotateOrientation
 * @brief  Returns a rotated orientation based upon rotation amount
 * @param  arg_name : arg_brief
 * @return Orientation : Rotated orientation
 * @throws None :
 */
Orientation
rotateOrientation(const Orientation& original, const Orientation& rotate)
{
  Orientation ret = original;
  switch(rotate)
  {
    case Orientation::eOrient1:
    {
      switch(original)
      {
        case Orientation::eOrient3:
        {
          ret = Orientation::eOrient0;
          break;
        }
        case Orientation::eOrient2:
        {
          ret = Orientation::eOrient3;
          break;
        }
        case Orientation::eOrient1:
        {
          ret = Orientation::eOrient2;
          break;
        }
        case Orientation::eOrient0:
        default:
        {
          ret = Orientation::eOrient1;
          break;
        }
      }
      break;
    }
    case Orientation::eOrient2:
    {
      switch(original)
      {
        case Orientation::eOrient3:
        {
          ret = Orientation::eOrient1;
          break;
        }
        case Orientation::eOrient2:
        {
          ret = Orientation::eOrient0;
          break;
        }
        case Orientation::eOrient1:
        {
          ret = Orientation::eOrient3;
          break;
        }
        case Orientation::eOrient0:
        default:
        {
          ret = Orientation::eOrient2;
          break;
        }
      }
      break;
    }
    case Orientation::eOrient3:
    {
      switch(original)
      {
        case Orientation::eOrient3:
        {
          ret = Orientation::eOrient2;
          break;
        }
        case Orientation::eOrient2:
        {
          ret = Orientation::eOrient1;
          break;
        }
        case Orientation::eOrient1:
        {
          ret = Orientation::eOrient0;
          break;
        }
        case Orientation::eOrient0:
        default:
        {
          ret = Orientation::eOrient3;
          break;
        }
      }
      break;
    }
    case Orientation::eOrient0:
    default:
    {
      // Seriously?
      break;
    }
  }

  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   getRotation
 * @brief  Creates a PuzzleConfig_t with a rotation
 * @param  TODO
 * @return PuzzleConfig_t : Rotated PuzzleConfig
 * @throws None
 */
PuzzleConfig
PuzzleConfig::getRotation(Orientation rotation)
const
{
  PuzzleConfig ret = *this; // Start with a copy

  switch(rotation)
  {
    case Orientation::eOrient1:
    {
      // 90deg clockwise
      ret.locations[0][0] = this->locations[2][0];
      ret.orientations[0][0] =
        rotateOrientation(this->orientations[2][0], rotation);

      ret.locations[0][1] = this->locations[1][0];
      ret.orientations[0][1] =
        rotateOrientation(this->orientations[1][0], rotation);

      ret.locations[0][2] = this->locations[0][0];
      ret.orientations[0][2] =
        rotateOrientation(this->orientations[0][0], rotation);

      ret.locations[1][0] = this->locations[2][1];
      ret.orientations[1][0] =
        rotateOrientation(this->orientations[2][1], rotation);

      ret.locations[1][2] = this->locations[0][1];
      ret.orientations[1][2] =
        rotateOrientation(this->orientations[0][1], rotation);

      ret.locations[2][0] = this->locations[2][2];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[2][2], rotation);

      ret.locations[2][1] = this->locations[1][2];
      ret.orientations[2][1] =
        rotateOrientation(this->orientations[1][2], rotation);

      ret.locations[2][2] = this->locations[0][2];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[0][2], rotation);

      break;
    }
    case Orientation::eOrient2:
    {
      // 180deg
      ret.locations[0][0] = this->locations[2][2];
      ret.orientations[0][0] =
        rotateOrientation(this->orientations[2][2], rotation);

      ret.locations[0][1] = this->locations[2][1];
      ret.orientations[0][1] =
        rotateOrientation(this->orientations[2][1], rotation);

      ret.locations[0][2] = this->locations[2][0];
      ret.orientations[0][2] =
        rotateOrientation(this->orientations[0][0], rotation);

      ret.locations[1][0] = this->locations[1][2];
      ret.orientations[1][0] =
        rotateOrientation(this->orientations[1][2], rotation);

      ret.locations[1][2] = this->locations[1][0];
      ret.orientations[1][2] =
        rotateOrientation(this->orientations[1][0], rotation);

      ret.locations[2][0] = this->locations[0][2];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[0][2], rotation);

      ret.locations[2][1] = this->locations[0][1];
      ret.orientations[2][1] =
        rotateOrientation(this->orientations[0][1], rotation);

      ret.locations[2][2] = this->locations[0][0];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[0][0], rotation);
      break;
    }
    case Orientation::eOrient3:
    {
      // 90deg counter-clockwise
      ret.locations[0][0] = this->locations[0][2];
      ret.orientations[0][0] =
        rotateOrientation(this->orientations[0][2], rotation);

      ret.locations[0][1] = this->locations[1][2];
      ret.orientations[0][1] =
        rotateOrientation(this->orientations[1][2], rotation);

      ret.locations[0][2] = this->locations[2][2];
      ret.orientations[0][2] =
        rotateOrientation(this->orientations[2][2], rotation);

      ret.locations[1][0] = this->locations[0][1];
      ret.orientations[1][0] =
        rotateOrientation(this->orientations[0][1], rotation);

      ret.locations[1][2] = this->locations[2][1];
      ret.orientations[1][2] =
        rotateOrientation(this->orientations[2][1], rotation);

      ret.locations[2][0] = this->locations[0][0];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[0][0], rotation);

      ret.locations[2][1] = this->locations[1][0];
      ret.orientations[2][1] =
        rotateOrientation(this->orientations[1][0], rotation);

      ret.locations[2][2] = this->locations[2][0];
      ret.orientations[2][2] =
        rotateOrientation(this->orientations[2][0], rotation);
      break;
    }
    case Orientation::eOrient0:
    default:
    {
      // No change
      break;
    }
  }

  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   PuzzleConfigEq
 * @brief  Returns TRUE if puzzle configurations are non-unique
 * @param  arg_name : arg_brief
 * @return bool : Returns TRUE if equivalent
 * @throws None
 */
bool
PuzzleConfigEq(const PuzzleConfig& lhs, const PuzzleConfig& rhs)
{
  bool ret = false;

  PuzzleConfig nonUniqueRhs[4] =
  {
    rhs,
    rhs.getRotation(Orientation::eOrient1),
    rhs.getRotation(Orientation::eOrient2),
    rhs.getRotation(Orientation::eOrient3)
  };

  for(int o=0; o<4; ++o)
  {
    bool eqFlag = true;
    for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
      {
        eqFlag = eqFlag && (lhs.locations[i][j] == nonUniqueRhs[o].locations[i][j]);
        eqFlag = eqFlag && (lhs.orientations[i][j] == nonUniqueRhs[o].orientations[i][j]);
        if(!eqFlag)
        {
          break;
        }
      }
      if(!eqFlag)
      {
        break;
      }
    }
    if(eqFlag)
    {
      ret = true;
      break;
    }
  }

  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   operator()
 * @brief  Comparitor function that indicates if lhs is strictly less than rhs
 * @param  lhs (const PuzzleConfig&) - left side of equivalence
 * @param  rhs (const PuzzleConfig&) - right side of equivalence
 * @return bool : Returns TRUE if the lhs < rhs
 * @throws None
 */
bool
PuzzleConfigCompare::operator()(const PuzzleConfig& lhs,
                                const PuzzleConfig& rhs)
{
  // Check equivalence
  if(PuzzleConfigEq(lhs,rhs))
  {
    return false;
  }

  // Check configurations first
  bool ret = false;
  std::int8_t tern = 0;

  for(int i=0; i<3; ++i)
  {
    for(int j=0; j<3; ++j)
    {
      if(lhs.locations[i][j] < rhs.locations[i][j])
      {
        tern = -1;
        ret = true;
      }
      else if(lhs.locations[i][j] < rhs.locations[i][j])
      {
        tern = 1;
        ret = false;
      }
      // else, must be equal

      if(tern !=0)
      {
        break;
      }
    }
    if(tern !=0)
    {
      break;
    }
  }

  // Check orientations
  if(tern == 0)
  {
    bool breakFlag = false;
    for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
      {
        if(lessthanOrientation(lhs.orientations[i][j], rhs.orientations[i][j]) !=
           lessthanOrientation(rhs.orientations[i][j], lhs.orientations[i][j])  )
        {
          // Not equal, so it's the final shot
          breakFlag = true;
          ret = lessthanOrientation(lhs.orientations[i][j], rhs.orientations[i][j]);
        }

        if(breakFlag)
        {
          break;
        }
      }
      if(breakFlag)
      {
        break;
      }
    }
  }

  return ret;
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
