/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Tile.cpp
 * @brief    Structure to contain partial object edge-detection maps for edges
 *           of tiles. This is the minimum necessary data to represent a tile to
 *           assess puzzle solutions.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "Tile.h"

#include <cstdlib>


//=== Constructors/Destructor ==================================================

/**
 * @name   Tile
 * @brief  Default construction of the Tile object.
 * @param  None
 * @return Tile : Constructed instance of Tile
 * @throws None
 */
Tile::Tile() :
  edge0(NULL),
  edge1(NULL),
  edge2(NULL),
  edge3(NULL),
  edgeLength(0)
{
  // Nothing to do here
}

//------------------------------------------------------------------------------

/**
 * @name   Tile
 * @brief  Construction of the Tile object with set edge length
 * @param  edgeLen (const std::uint32_t&) - length of one side of the image
 *         in pixels
 * @return Tile : Constructed instance of Tile with set edges
 * @throws None
 */
Tile::Tile(const std::uint32_t& edgeLen) :
    edge0(nullptr),
    edge1(nullptr),
    edge2(nullptr),
    edge3(nullptr),
    edgeLength(edgeLen)
{
  edge0 = new std::uint8_t[edgeLen];
  edge1 = new std::uint8_t[edgeLen];
  edge2 = new std::uint8_t[edgeLen];
  edge3 = new std::uint8_t[edgeLen];
}

//------------------------------------------------------------------------------

/**
 * @name   Tile
 * @brief  Deep-copy Construction of the Tile object
 * @param  original (const Tile&) - Original copy of the tile
 *         in pixels
 * @return Tile : Constructed instance of Tile
 * @throws None
 */
Tile::Tile(const Tile& original) :
      edge0(nullptr),
      edge1(nullptr),
      edge2(nullptr),
      edge3(nullptr),
      edgeLength(original.edgeLength)
{
  edge0 = new std::uint8_t[original.edgeLength];
  edge1 = new std::uint8_t[original.edgeLength];
  edge2 = new std::uint8_t[original.edgeLength];
  edge3 = new std::uint8_t[original.edgeLength];

  // Copy-time!
  for(std::uint32_t i=0; i< original.edgeLength; ++i)
  {
    edge0[i] = original.edge0[i];
    edge1[i] = original.edge1[i];
    edge2[i] = original.edge2[i];
    edge3[i] = original.edge3[i];
  }
}


//------------------------------------------------------------------------------

/**
 * @name   ~Tile
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
Tile::~Tile()
{
  if(edgeLength > 0)
  {
    delete[] edge0; edge0 = NULL;
    delete[] edge1; edge1 = NULL;
    delete[] edge2; edge2 = NULL;
    delete[] edge3; edge3 = NULL;
    edgeLength = 0;
  }
}

//=== Public Methods ===========================================================

/**
 * @name   getEdge
 * @brief  Returns reference to the tile edge provided side and orientation
 * @param  orientation (const Orientation&) - orientation of the tile
 * @param  side (const TileSide&) - which side of the tile to retrieve
 *         relative to specified orientation
 * @return uint8_t* : Requested edge map with size edgeLength
 * @throws None
 */
std::uint8_t*
Tile::getEdge(const Orientation& rot, const TileSide& side)
{
  // Rectify orientation, then get the appropriate side
  std::uint8_t sides[4] = {0,1,2,3};
  switch(rot)
  {
    case Orientation::eOrient1:
    {
      sides[0] = 3;
      sides[1] = 0;
      sides[2] = 1;
      sides[3] = 2;
      break;
    }
    case Orientation::eOrient2:
    {
      sides[0] = 2;
      sides[1] = 3;
      sides[2] = 0;
      sides[3] = 1;
      break;
    }
    case Orientation::eOrient3:
    {
      sides[0] = 1;
      sides[1] = 2;
      sides[2] = 3;
      sides[3] = 0;
      break;
    }
    case Orientation::eOrient0:
    default:
    {
      break;
    }
  }

  // Figure out which side we are really getting
  std::uint8_t eIdx = 0;
  switch(side)
  {
    case TileSide::eRight:
    {
      eIdx = sides[1];
      break;
    }
    case TileSide::eBottom:
    {
      eIdx = sides[2];
      break;
    }
    case TileSide::eLeft:
    {
      eIdx = sides[3];
      break;
    }
    case TileSide::eTop:
    default:
    {
      eIdx = sides[0];
      break;
    }
  }

  // Get the side
  std::uint8_t* ret = NULL;
  switch(eIdx)
  {
    case 0:
    {
      ret = edge0;
      break;
    }
    case 1:
    {
      ret = edge1;
      break;
    }
    case 2:
    {
      ret = edge2;
      break;
    }
    case 3:
    {
      ret = edge3;
      break;
    }
    default:
    {
      ret = NULL;
      break;
    }
  }
  return ret;
}

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
