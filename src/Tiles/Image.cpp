/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Image.cpp
 * @brief    Describes a structure for storing bitmap images representing puzzle
 *           tiles and provides minimal image processing.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "Image.h"

#include <fstream>
#include <cmath>
#include <iostream>


//=== Defines ==================================================================

#define BYTES_PER_PIXEL (3)


//=== Constructors/Destructor ==================================================

/**
 * @name   Image
 * @brief  Default construction of the Image object.
 * @param  None
 * @return Image : Constructed instance of Image with empty image
 * @throws None
 */
Image::Image() :
  tileLength(0),
  rawCh1(nullptr),
  rawCh2(nullptr),
  rawCh3(nullptr),
  cvCh1(nullptr),
  cvCh2(nullptr),
  cvCh3(nullptr)
{
  // Nothing to do here
}

//------------------------------------------------------------------------------

/**
 * @name   Image
 * @brief  Default construction of the Image object.
 * @param  filename (const std::string&) - Name of puzzle tile image file
 * @return Image : Constructed instance of Image
 * @throws None
 */
Image::Image(const std::string& filename):
  tileLength(0),
  rawCh1(nullptr),
  rawCh2(nullptr),
  rawCh3(nullptr),
  cvCh1(nullptr),
  cvCh2(nullptr),
  cvCh3(nullptr)

{
  // Set up containers
  std::ifstream imgFile(filename.c_str(), std::ifstream::binary);
  imgFile.seekg(0,std::ifstream::end);
  const std::size_t filesize = (std::size_t)(imgFile.tellg());

  const std::size_t numPixels = filesize / BYTES_PER_PIXEL;
  const std::size_t tlen = (const std::size_t)(std::sqrt((double)(numPixels)));
  tileLength = (std::uint32_t)(tlen);

  make2dByteArray(&rawCh1, tlen);
  make2dByteArray(&rawCh2, tlen);
  make2dByteArray(&rawCh3, tlen);
  make2dFloatArray(&cvCh1, tlen);
  make2dFloatArray(&cvCh2, tlen);
  make2dFloatArray(&cvCh3, tlen);

  // Read the data
  std::uint8_t* data = new std::uint8_t[filesize];
  imgFile.seekg(0, std::ifstream::beg);
  imgFile.read((char*)data, filesize);

  // Strip the data
  for(std::uint32_t didx=0; didx < tileLength*tileLength; didx+=3)
  {
    std::uint32_t ridx = didx / tileLength;
    std::uint32_t cidx = didx % tileLength;
    rawCh1[ridx][cidx] = data[didx];
    rawCh2[ridx][cidx] = data[didx+1];
    rawCh3[ridx][cidx] = data[didx+2];

    cvCh1[ridx][cidx] =
      (((float)rawCh1[ridx][cidx]) - 128.0f) / 256.0f;
    cvCh2[ridx][cidx] =
      (((float)rawCh2[ridx][cidx]) - 128.0f) / 256.0f;
    cvCh3[ridx][cidx] =
      (((float)rawCh3[ridx][cidx]) - 128.0f) / 256.0f;
  }

  // Cleanup
  imgFile.close();
}

//------------------------------------------------------------------------------

/**
 * @name   Image
 * @brief  Constructor of an Image object from OpenCV data structure
 * @param  imgData (cv::Mat) - Description of the tile data
 * @return Image : Constructed instance of Image
 * @throws None
 */
Image::Image(cv::Mat imgData):
    tileLength(0),
    rawCh1(nullptr),
    rawCh2(nullptr),
    rawCh3(nullptr),
    cvCh1(nullptr),
    cvCh2(nullptr),
    cvCh3(nullptr)
{
  tileLength = (std::uint32_t)(imgData.rows);
  make2dFloatArray(&cvCh1, tileLength);
  make2dFloatArray(&cvCh2, tileLength);
  make2dFloatArray(&cvCh3, tileLength);

  // Not caring about the raw image data at this point

  for(std::uint32_t i=0; i<tileLength; ++i)
  {
    for(std::uint32_t j=0; j<tileLength; ++j)
    {
      cvCh1[i][j] = imgData.at<cv::Vec2f>(i,j)[0] - 128.0;
      cvCh2[i][j] = imgData.at<cv::Vec2f>(i,j)[1] - 128.0;
      cvCh3[i][j] = imgData.at<cv::Vec2f>(i,j)[2] - 128.0;
    }
  }
}

//------------------------------------------------------------------------------

/**
 * @name   ~Image
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
Image::~Image()
{
  if(tileLength > 0)
  {
    for(std::size_t idx=0; idx < (std::size_t)tileLength; ++idx)
    {
      if(rawCh1 != nullptr) // Best way to check which constructor built this
      {
        delete[] rawCh1[idx];
        delete[] rawCh2[idx];
        delete[] rawCh3[idx];
      }
      delete[] cvCh1[idx];
      delete[] cvCh2[idx];
      delete[] cvCh3[idx];
    }
    delete[] rawCh1; rawCh1 = nullptr;
    delete[] rawCh2; rawCh2 = nullptr;
    delete[] rawCh3; rawCh3 = nullptr;
    delete[] cvCh1;  cvCh1  = nullptr;
    delete[] cvCh2;  cvCh2  = nullptr;
    delete[] cvCh3;  cvCh3  = nullptr;
    tileLength = 0;
  }
}


//=== Public Methods ===========================================================

/**
 * @name   generateTile
 * @brief  Generates a Tile from the image of the puzzle tile
 * @param  None
 * @return Tile : See Tile.h
 * @throws None
 */
Tile
Image::generateTile()
{
  Tile ret(tileLength);
  calculateEdgeDiscontinuities(ret);
  return ret;
}


//=== Private Methods ==========================================================

/**
 * @name   calculateEdgeDiscontinuities
 * @brief  Calculates location of edge points along tile edges; in other
 *         words, given a partial object crossing a tile edge, returns the
 *         location(s) of the edges of the partial object along the tile
 *         edge.
 *         Note that the edges are measured in the following directions:
 *
 *            *-->         *
 *             +----------+|
 *             |          |v
 *             |     A    |
 *             |    /|\   |
 *             |     |    |
 *            A|          |
 *            |+----------+
 *            *         <--*
 *
 * @param  edgeMap (Tile&) - Reference container to populate edgemaps
 * @return void - Pseudo-returns to edgeMap
 * @throws None
 */
void
Image::calculateEdgeDiscontinuities(Tile& edgeMap)
{
  float **detector = generateLaplacianMat();

  static const float edThreshold = 0.01;

  float** ch1Buf = NULL;
  float** ch2Buf = NULL;
  float** ch3Buf = NULL;
  make2dFloatArray(&ch1Buf, 9);
  make2dFloatArray(&ch2Buf, 9);
  make2dFloatArray(&ch3Buf, 9);

  float ec1 = 0.0;
  float ec2 = 0.0;
  float ec3 = 0.0;

  // Calculate top & bottom edge maps
  for(std::uint32_t cidx=0; cidx < tileLength; ++cidx)
  {
    // Top edge
    getImagePatch(&ch1Buf, &ch2Buf, &ch3Buf, 0, cidx);
    ec1 = correlatePatches(ch1Buf, detector);
    ec2 = correlatePatches(ch2Buf, detector);
    ec3 = correlatePatches(ch3Buf, detector);

    if(std::fabs(ec1) < edThreshold ||
       std::fabs(ec2) < edThreshold ||
       std::fabs(ec3) < edThreshold )
    {
      edgeMap.edge0[cidx] = 1;
    }
    else
    {
      edgeMap.edge0[cidx] = 0;
    }

    // Bottom edge in reverse order
    getImagePatch(&ch1Buf, &ch2Buf, &ch3Buf, tileLength-1, tileLength - cidx);
    ec1 = correlatePatches(ch1Buf, detector);
    ec2 = correlatePatches(ch2Buf, detector);
    ec3 = correlatePatches(ch3Buf, detector);

    if(std::fabs(ec1) < edThreshold ||
       std::fabs(ec2) < edThreshold ||
       std::fabs(ec3) < edThreshold )
    {
      edgeMap.edge2[cidx] = 1;
    }
    else
    {
      edgeMap.edge2[cidx] = 0;
    }
  }

  // Calculate right & left edge map
  for(std::uint32_t ridx=0; ridx < tileLength; ++ridx)
  {
    // Left edge in reverse order
    getImagePatch(&ch1Buf, &ch2Buf, &ch3Buf, tileLength - ridx, 0);
    ec1 = correlatePatches(ch1Buf, detector);
    ec2 = correlatePatches(ch2Buf, detector);
    ec3 = correlatePatches(ch3Buf, detector);

    if(std::fabs(ec1) < edThreshold ||
       std::fabs(ec2) < edThreshold ||
       std::fabs(ec3) < edThreshold )
    {
      edgeMap.edge1[ridx] = 1;
    }
    else
    {
      edgeMap.edge1[ridx] = 0;
    }

    // Right edge
    getImagePatch(&ch1Buf, &ch2Buf, &ch3Buf, ridx, tileLength-1);
    ec1 = correlatePatches(ch1Buf, detector);
    ec2 = correlatePatches(ch2Buf, detector);
    ec3 = correlatePatches(ch3Buf, detector);

    if(std::fabs(ec1) < edThreshold ||
       std::fabs(ec2) < edThreshold ||
       std::fabs(ec3) < edThreshold )
    {
      edgeMap.edge3[ridx] = 1;
    }
    else
    {
      edgeMap.edge3[ridx] = 0;
    }
  }


  // Clean up the 2d array
  for(int i=0; i<9; ++i)
  {
    delete[] ch1Buf[i];
    delete[] ch2Buf[i];
    delete[] ch3Buf[i];
  }
  delete[] ch1Buf;
  delete[] ch2Buf;
  delete[] ch3Buf;

//  TODO: delete[] detector;
}

//------------------------------------------------------------------------------

/**
 * @name   make2dByteArray
 * @brief  Creates a 2D array of bytes
 * @param  byteArray (std::uint8_t**) - reference to byte array
 * @param  size (const std::size_t&) - size of the array
 * @returnNone
 * @throws None
 */
void
Image::make2dByteArray(std::uint8_t*** byteArray, const std::size_t& size)
{
  (*byteArray) = new std::uint8_t*[size];
  for(std::size_t idx=0; idx < size; ++idx)
  {
    (*byteArray)[idx] = new std::uint8_t[size];
  }
}

//------------------------------------------------------------------------------

/**
 * @name   make2dFloatArray
 * @brief  Creates a 2D array of floats
 * @param  floatArray (float**) - reference to float array
 * @param  size (const std::size_t&) - size of the array
 * @returnNone
 * @throws None
 */
void
Image::make2dFloatArray(float*** floatArray, const std::size_t& size)
{
  (*floatArray) = new float*[size];
  for(std::size_t idx=0; idx < size; ++idx)
  {
    (*floatArray)[idx] = new float[size];
  }
}

//------------------------------------------------------------------------------

/**
 * @name   getImagePatch
 * @brief  Populates a 9x9 float array with an image patch centered at the
 *         specified (row,col) coordinates. For out-of-bounds areas,
 *         replicates the nearest image value.
 * @param  ch1 (float***) - Reference to chroma channel 1 double-array
 * @param  ch2 (float***) - Reference to chroma channel 2 double-array
 * @param  ch3 (float***) - Reference to chroma channel 3 double-array
 * @param  row (const std::uint32_t&) - center pixel location row coordinate
 * @param  col (const std::uint32_t&) - center pixel location col coordinate
 * @return void : Pseudo-returns are ch1, ch2, and ch3
 * @throws None
 */
void
Image::getImagePatch(float*** ch1, float*** ch2, float*** ch3,
              const std::uint32_t& row, const std::uint32_t& col)
{
  for(int i=0, ridx=(int)row-4; i<9; ++i, ++ridx)
  {
    int r = ridx;
    if(r < 0)                { r = 0;            }
    else if(r >= (int)tileLength) { r = (int)tileLength-1; }

    for(int j=0, cidx=(int)col-4; j<9; ++j, ++cidx)
    {
      int c = cidx;
      if(c < 0)                { c = 0;            }
      else if(c >= (int)tileLength) { c = (int)tileLength-1; }

      (*ch1)[i][j] = cvCh1[r][c];
      (*ch2)[i][j] = cvCh2[r][c];
      (*ch3)[i][j] = cvCh3[r][c];
    }
  }
}

//------------------------------------------------------------------------------

/**
 * @name   correlatePatches
 * @brief  Applies the correlation operator to two 9x9 image patches
 * @param  a (float**) - 2D float array
 * @param  b (float**) - 2D float array
 * @return float : Value of the correlation operation
 * @throws None
 */
float
Image::correlatePatches(float** a, float** b)
{
  float ret = 0.0;
  for(int i=0; i<9; ++i)
  {
    for(int j=0; j<9; ++j)
    {
      ret += ((a[i][j]) * (b[i][j]));
    }
  }

  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   generateLaplacianMat
 * @brief  Generates a 9x9 Laplacian surface used for edge detection
 * @return float** : Reference to the stored matrix
 * @throws None
 */
float**
Image::generateLaplacianMat()
{
  // NOTE: Values taken from Figure 3 in
  //       https://http://homepages.inf.ed.ac.uk/rbf/HIPR2/log.htm
  static const float laplacian[9][9] =
  {
    { 0.0,   1.0,   1.0,   2.0,   2.0,   2.0,   1.0,   1.0,   0.0},
    { 1.0,   2.0,   4.0,   5.0,   5.0,   5.0,   4.0,   2.0,   1.0},
    { 1.0,   4.0,   5.0,   3.0,   0.0,   3.0,   5.0,   4.0,   1.0},
    { 2.0,   5.0,   3.0, -12.0, -24.0, -12.0,   3.0,   5.0,   2.0},
    { 2.0,   5.0,   0.0, -24.0, -40.0, -24.0,   0.0,   5.0,   2.0},
    { 2.0,   5.0,   3.0, -12.0, -24.0, -12.0,   3.0,   5.0,   2.0},
    { 1.0,   4.0,   5.0,   3.0,   0.0,   3.0,   5.0,   4.0,   1.0},
    { 1.0,   2.0,   4.0,   5.0,   5.0,   5.0,   4.0,   2.0,   1.0},
    { 0.0,   1.0,   1.0,   2.0,   2.0,   2.0,   1.0,   1.0,   0.0}
  };

  float** ret = nullptr;
  make2dFloatArray(&ret, 9);

  for(std::size_t i=0; i<9; ++i)
  {
    for(std::size_t j=0; j<9; ++j)
    {
      ret[i][j] = laplacian[i][j];
    }
  }

  return ret;
}

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
