/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     PuzzleConfig.h
 * @brief    Declares puzzle configuration primatives.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef PUZZLECONFIG_H_
#define PUZZLECONFIG_H_

//=== Includes =================================================================

#include <cstdint>


//=== Type Definitions =========================================================

/**
 * @enum  Orientation
 * @brief Orientation of the edge maps; see Readme.md for details.
 */
enum class Orientation
{
  eOrient0 = 0x0F, /// @enum eOrient0 Represents Orientation 0
  eOrient1 = 0x33, /// @enum eOrient0 Represents Orientation 1
  eOrient2 = 0x55, /// @enum eOrient0 Represents Orientation 2
  eOrient3 = 0x6a  /// @enum eOrient0 Represents Orientation 3
};

//------------------------------------------------------------------------------

/**
 * @name   lessthanOrientation
 * @brief  Computes if lhs < rhs
 * @param  lhs (const Orientation&) - left side of less-than comparison
 * @param  rhs (const Orientation&) - right side of less-than comparison
 * @return bool : Returns TRUE if lhs < rhs
 * @throws None
 */
bool
lessthanOrientation(const Orientation& lhs, const Orientation& rhs);

//------------------------------------------------------------------------------

/**
 * @name   rotateOrientation
 * @brief  Returns a rotated orientation based upon rotation amount
 * @param  arg_name : arg_brief
 * @return Orientation : Rotated orientation
 * @throws None :
 */
Orientation
rotateOrientation(const Orientation& original, const Orientation& rotate);

//==============================================================================

/**
 * @enum  TileSide
 * @brief Identifier for the side of a tile independent of orientation
 */
enum class TileSide
{
  eTop    = 0x96, /// @enum eTop Represents Top edge of tile
  eRight  = 0xab, /// @enum eRight Represents Right edge of tile
  eBottom = 0xdb, /// @enum eBottom Represents Bottom edge of tile
  eLeft   = 0xed  /// @enum eLeft Represents Left edge of tile
};

//==============================================================================

/**
 * @struct PuzzleConfiguration
 * @brief  Describes a configuration of puzzle pieces
 */
typedef struct PuzzleConfig_t
{
  //--- Members ----------------------------------------------------------------

  /**
   * @name   locations
   * @brief  Describes where each piece's location is
   * @units  N/A
   * @bounds N/A
   */
  std::uint8_t locations[3][3];

  /**
   * @name   orientations
   * @brief  Describes how each piece is oriented
   * @units  N/A
   * @bounds N/a
   */
  Orientation orientations[3][3];

  //--- Methods ----------------------------------------------------------------

  /**
   * @name   getRotation
   * @brief  Creates a PuzzleConfig_t with a rotation
   * @param  None :
   * @return PuzzleConfig_t : Rotated PuzzleConfig
   * @throws None :
   */
  PuzzleConfig_t
  getRotation(Orientation rotation)
  const;

} PuzzleConfig;

//------------------------------------------------------------------------------

/**
 * @name   PuzzleConfigEq
 * @brief  Returns TRUE if puzzle configurations are non-unique
 * @param  arg_name : arg_brief
 * @return bool : Returns TRUE if equivalent
 * @throws None
 */
bool
PuzzleConfigEq(const PuzzleConfig& lhs, const PuzzleConfig& rhs);

//==============================================================================

/**
 * @struct PuzzleConfigCompare
 * @brief  brief
 */
typedef struct PuzzleConfigCompare_t
{
  //--- Methods ----------------------------------------------------------------
  /**
   * @name   operator()
   * @brief  Comparitor function that indicates if lhs is strictly less than rhs
   * @param  lhs (const PuzzleConfig&) - left side of equivalence
   * @param  rhs (const PuzzleConfig&) - right side of equivalence
   * @return bool : Returns TRUE if the lhs < rhs
   * @throws None
   */
  bool
  operator()(const PuzzleConfig& lhs, const PuzzleConfig& rhs);
} PuzzleConfigCompare;


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
