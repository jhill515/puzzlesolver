/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Image.h
 * @brief    Describes a structure for storing bitmap images representing puzzle
 *           tiles and provides minimal image processing.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef IMAGE_H_
#define IMAGE_H_

//=== Includes =================================================================

#include <string>
#include <cstdint>
#include <opencv2/opencv.hpp>

#include "Tile.h"


//=== Type Definitions =========================================================

/**
 * @class   Image
 * @extends None
 * @brief   Short-lived image container and preprocessor.
 *          When reading from the file, it is assuming that there are three
 *          chroma channels and that they are represented by interleaved bytes.
 */
class Image
{
  public: /// @publicsection
    //--- Constructors/Destructor ----------------------------------------------

    /**
     * @name   Image
     * @brief  Default construction of the Image object.
     * @param  None
     * @return Image : Constructed instance of Image with empty image
     * @throws None
     */
    Image();

    /**
     * @name   Image
     * @brief  Constructor of an Image object from a data file
     * @param  filename (const std::string&) - Name of puzzle tile image file
     * @return Image : Constructed instance of Image
     * @throws None
     */
    Image(const std::string& filename);

    /**
     * @name   Image
     * @brief  Constructor of an Image object from OpenCV data structure
     * @param  imgData (cv::Mat) - Description of the tile data
     * @return Image : Constructed instance of Image
     * @throws None
     */
    Image(cv::Mat imgData);

    /**
     * @name   ~Image
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~Image();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   generateTile
     * @brief  Generates a Tile from the image of the puzzle tile
     * @param  None
     * @return Tile : See Tile.h
     * @throws None
     */
    Tile
    generateTile();


  private: /// @privatesection
    //--- Private Members ------------------------------------------------------

    /**
     * @name   tileLength
     * @brief  Length of a puzzle tile
     * @units  pixels
     * @bounds Must be greater than six, but unchecked
     */
    std::uint32_t tileLength;

    /**
     * @name   rawCh1
     * @brief  Reference to the array of the first chroma channel as read from
     *         the image file.
     * @units  N/A
     * @bounds N/A
     * @note   Since the specification is 3 channels at 24-bits per pixel, this
     *         means that the channel is bound between 0-255 inclusively.
     * @note   The array itself is length tileLength*tileLength.
     */
    std::uint8_t** rawCh1;

    /**
     * @name   rawCh2
     * @brief  Reference to the array of the second chroma channel as read from
     *         the image file.
     * @units  N/A
     * @bounds N/A
     * @note   Since the specification is 3 channels at 24-bits per pixel, this
     *         means that the channel is bound between 0-255 inclusively.
     * @note   The array itself is length tileLength*tileLength.
     */
    std::uint8_t** rawCh2;

    /**
     * @name   rawCh3
     * @brief  Reference to the array of the third chroma channel as read from
     *         the image file.
     * @units  N/A
     * @bounds N/A
     * @note   Since the specification is 3 channels at 24-bits per pixel, this
     *         means that the channel is bound between 0-255 inclusively.
     * @note   The array itself is length tileLength*tileLength.
     */
    std::uint8_t** rawCh3;

    /**
     * @name   cvCh1
     * @brief  Reference to the array of the first chroma channel as converted
     *         from the image file data.
     * @units  N/A
     * @bounds N/A
     * @note   Pixel channels are bound to the interval of -1.0 to 1.0 with -1.0
     *         mapping to raw=0, 0.0 mapping to raw=128, and 1.0 mapping to
     *         raw=255.
     * @note   The array itself is length tileLength*tileLength
     */
    float** cvCh1;

    /**
     * @name   rawCh2
     * @brief  Reference to the array of the second chroma channel as converted
     *         from the image file data.
     * @units  N/A
     * @bounds N/A
     * @note   Pixel channels are bound to the interval of -1.0 to 1.0 with -1.0
     *         mapping to raw=0, 0.0 mapping to raw=128, and 1.0 mapping to
     *         raw=255.
     * @note   The array itself is length tileLength*tileLength
     */
    float** cvCh2;

    /**
     * @name   rawCh3
     * @brief  Reference to the array of the third chroma channel as converted
     *         from the image file data.
     * @units  N/A
     * @bounds N/A
     * @note   Pixel channels are bound to the interval of -1.0 to 1.0 with -1.0
     *         mapping to raw=0, 0.0 mapping to raw=128, and 1.0 mapping to
     *         raw=255.
     * @note   The array itself is length tileLength*tileLength
     */
    float** cvCh3;


    //--- Private Methods ------------------------------------------------------

    /**
     * @name   calculateEdgeDiscontinuities
     * @brief  Calculates location of edge points along tile edges; in other
     *         words, given a partial object crossing a tile edge, returns the
     *         location(s) of the edges of the partial object along the tile
     *         edge.
     *         Note that the edges are measured in the following directions:
     *
     *            *-->         *
     *             +----------+|
     *             |          |v
     *             |     A    |
     *             |    /|\   |
     *             |     |    |
     *            A|          |
     *            |+----------+
     *            *         <--*
     *
     * @param  edgeMap (Tile&) - Reference container to populate edgemaps
     * @return void - Pseudo-returns to edgeMap
     * @throws None
     */
    void
    calculateEdgeDiscontinuities(Tile& edgeMap);

    /**
     * @name   make2dByteArray
     * @brief  Creates a 2D array of bytes
     * @param  byteArray (std::uint8_t**) - reference to byte array
     * @param  size (const std::size_t&) - size of the array
     * @returnNone
     * @throws None
     */
    void
    make2dByteArray(std::uint8_t*** byteArray, const std::size_t& size);

    /**
     * @name   make2dFloatArray
     * @brief  Creates a 2D array of floats
     * @param  floatArray (float**) - reference to float array
     * @param  size (const std::size_t&) - size of the array
     * @returnNone
     * @throws None
     */
    void
    make2dFloatArray(float*** floatArray, const std::size_t& size);

    /**
     * @name   getImagePatch
     * @brief  Populates a 9x9 float array with an image patch centered at the
     *         specified (row,col) coordinates. For out-of-bounds areas,
     *         replicates the nearest image value.
     * @param  ch1 (float***) - Reference to chroma channel 1 double-array
     * @param  ch2 (float***) - Reference to chroma channel 2 double-array
     * @param  ch3 (float***) - Reference to chroma channel 3 double-array
     * @param  row (const std::uint32_t&) - center pixel location row coordinate
     * @param  col (const std::uint32_t&) - center pixel location col coordinate
     * @return void : Pseudo-returns are ch1, ch2, and ch3
     * @throws None
     */
    void
    getImagePatch(float*** ch1, float*** ch2, float*** ch3,
                  const std::uint32_t& row, const std::uint32_t& col);

    /**
     * @name   correlatePatches
     * @brief  Applies the correlation operator to two 9x9 image patches
     * @param  a (float**) - 2D float array
     * @param  b (float**) - 2D float array
     * @return float : Value of the correlation operation
     * @throws None
     */
    float
    correlatePatches(float** a, float** b);

    /**
     * @name   generateLaplacianMat
     * @brief  Generates a 9x9 Laplacian surface used for edge detection
     * @return float** : Reference to the stored matrix
     * @throws None
     */
    float**
    generateLaplacianMat();
};

#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill iV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
