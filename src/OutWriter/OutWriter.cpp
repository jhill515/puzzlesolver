/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV>; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     OutWriter.cpp
 * @brief    Declaration of output file interface used by solution processors.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "OutWriter.h"

#include <iostream>


//=== Constructors/Destructor ==================================================

/**
 * @name   OutWriter
 * @brief  Default construction of the OutWriter object.
 * @param  None
 * @return OutWriter : Constructed instance of OutWriter
 * @throws None
 */
OutWriter::OutWriter() :
  outfile("puzzlesolver_results.txt", std::ofstream::out | std::ofstream::trunc),
  writable(false)
{
  if(!outfile)
  {
    std::cerr << "ERROR: Failed to open puzzlesolver_results.txt!\n";
  }
  else
  {
    outfile << "Pieces   Orientations\n"
            << "---------------------\n";
    writable = true;
  }
}

//------------------------------------------------------------------------------

/**
 * @name   ~OutWriter
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
OutWriter::~OutWriter()
{
  if(writable)
  {
    closeFile();
  }
}


//=== Public Methods ===========================================================

/**
 * @name   getInstance
 * @brief  Returns and instance of the singleton
 * @param  None
 * @return OutWriter& : Instance of the singleton
 * @throws None
 */
OutWriter&
OutWriter::getInstance()
{
  static OutWriter instance;
  return instance;
}

//------------------------------------------------------------------------------

/**
 * @name   writeSolution
 * @brief  Records a puzzle configuration as a solution
 * @param  solution (const PuzzleConfig&) - puzzle configuration to be
 *         recorded
 * @return void
 * @throws None
 */
void
OutWriter::writeSolution(const PuzzleConfig& solution)
{
  lock.lock();

  if(writable)
  {
    for(int i=0; i < 3; ++i)
    {
      // Write places row
      for(int j=0; j < 3; ++j)
      {
        outfile << solution.locations[i][j] << " ";
      }

      outfile << "   ";

      // Write orientations row
      for(int j=0; j < 3; ++j)
      {
        Orientation o = solution.orientations[i][j];
        switch(o)
        {
          case Orientation::eOrient0:
          {
            outfile << "0 ";
            break;
          }
          case Orientation::eOrient1:
          {
            outfile << "1 ";
            break;
          }
          case Orientation::eOrient2:
          {
            outfile << "2 ";
            break;
          }
          case Orientation::eOrient3:
          {
            outfile << "3 ";
            break;
          }
          default:
          {
            // Shouldn't ever get here
            std::cerr << "ERROR: Acting on unknown orientation! Ignoring print...\n";
            break;
          }
        }
      }
      outfile << "\n";
    }
    outfile << "\n";
  }

  lock.unlock();
}

//------------------------------------------------------------------------------

/**
 * @name   closeFile
 * @brief  Closes the output file; to be executed at program completion
 * @param  None
 * @return void
 * @throws None
 */
void
OutWriter::closeFile()
{
  if(writable)
  {
    lock.lock();
    outfile.close();
    writable = false;
    lock.unlock();
  }
}

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
