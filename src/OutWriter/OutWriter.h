/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV>; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     OutWriter.h
 * @brief    Declaration of output file interface used by solution processors.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef OUTWRITER_H_
#define OUTWRITER_H_

//=== Includes  ================================================================

#include <fstream>
#include <mutex>

#include "PuzzleConfig.h"


//=== Type Definitions =========================================================

/**
 * @class   OutWriter
 * @extends None
 * @brief   Singleton instance of output file writer
 */
class OutWriter
{
  public: /// @publicsection
    //--- Destructor -----------------------------------------------------------

    /**
     * @name   ~OutWriter
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~OutWriter();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   getInstance
     * @brief  Returns and instance of the singleton
     * @param  None
     * @return OutWriter& : Instance of the singleton
     * @throws None
     */
    static
    OutWriter&
    getInstance();

    /**
     * @name   writeSolution
     * @brief  Records a puzzle configuration as a solution
     * @param  solution (const PuzzleConfig&) - puzzle configuration to be
     *         recorded
     * @return void
     * @throws None
     */
    void
    writeSolution(const PuzzleConfig& solution);

    /**
     * @name   closeFile
     * @brief  Closes the output file; to be executed at program completion
     * @param  None
     * @return void
     * @throws None
     */
    void
    closeFile();


  private: /// @privatesection
    //--- Private Members ------------------------------------------------------

    /**
     * @name   outfile
     * @brief  Output file
     * @units  N/A
     * @bounds N/A
     */
    std::ofstream outfile;

    /**
     * @name   lock
     * @brief  Lock to ensure only single thread is writing a solution at a time
     * @units  N/A
     * @bounds N/A
     */
    std::mutex lock;

    /**
     * @name   writable
     * @brief  Flag to indicate if the file is open to writing
     * @units  N/A
     * @bounds N/A
     */
    bool writable;


    //--- Constructor ----------------------------------------------------------

    /**
     * @name   OutWriter
     * @brief  Default construction of the OutWriter object.
     * @param  None
     * @return OutWriter : Constructed instance of OutWriter
     * @throws None
     */
    OutWriter();

};


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
