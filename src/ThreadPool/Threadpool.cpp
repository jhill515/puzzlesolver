/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Threadpool.cpp
 * @brief    Implements a manager for functor threads
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "Threadpool.h"

#include "PuzzleConfig.h"
#include "Taskmaster.h"


//=== Constructors/Destructor ==================================================

/**
 * @name   Threadpool
 * @brief  Default construction of the Threadpool object.
 * @param  None
 * @return Threadpool : Constructed instance of Threadpool
 * @throws None
 */
Threadpool::Threadpool() :
  threads(0)
{
  // Nothing to do here
}

//------------------------------------------------------------------------------

/**
 * @name   Threadpool
 * @brief  Default construction of the Threadpool object.
 * @param  None
 * @return Threadpool : Constructed instance of Threadpool
 * @throws None
 */
Threadpool::Threadpool(const std::uint32_t& threadCount) :
  threads(threadCount)
{
  // Nothing to do here
}

//------------------------------------------------------------------------------

/**
 * @name   ~Threadpool
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
Threadpool::~Threadpool()
{
  std::vector<PuzzleConfigProcessor>::iterator iter;
  for(iter = threads.begin(); iter != threads.end(); ++iter)
  {
    iter->stop();
  }
}


//=== Public Methods ===========================================================

/**
 * @name   setTiles
 * @brief  Sets the tiles structure for each thread
 * @param  puzzlePieces (Tile*) - Reference to a 9-element array of tiles
 * @return void
 * @throws None
 */
void
Threadpool::setTiles(Tile* puzzlePieces)
{
  std::vector<PuzzleConfigProcessor>::iterator iter;
  for(iter = threads.begin(); iter != threads.end(); ++iter)
  {
    iter->setTiles(puzzlePieces);
  }
}

//------------------------------------------------------------------------------

/**
 * @name   launchThreads
 * @brief  Launches all threads, monitors for task completion, and
 *         relaunches if there are more tasks to accomplish.
 * @param  arg_name : arg_brief
 * @return return_type : ret_brief
 * @throws except_type : except_brief
 */
void
Threadpool::launchThreads()
{
  std::vector<PuzzleConfigProcessor>::iterator iter;
  for(iter = threads.begin(); iter != threads.end(); ++iter)
  {
    iter->start();
  }
}

//------------------------------------------------------------------------------

/**
 * @name   monitorThreads
 * @brief  Monitors threads and provides them with tasks if available
 * @param  None
 * @return void
 * @throws None
 */
void
Threadpool::monitorThreads()
{
  bool workToDo = false;
  std::vector<PuzzleConfigProcessor>::iterator iter;
  Taskmaster& driver = Taskmaster::getInstance();

  do
  {
    workToDo = driver.hasMoreWork();
    if(workToDo)
    {
      // Get next task cable
      PuzzleConfig workCable = driver.getWork();

      // Find a worker and give it work!
      do
      {
        bool wFlag = true;
        for(iter = threads.begin(); iter != threads.end() && wFlag; ++iter)
        {
          if(!(iter->isProcessing()))
          {
            iter->setPuzzleConfig(workCable);
            wFlag = false;
          }
        }
      }while(iter == threads.end()); // If looping, all threads must be working
    }
  }while(workToDo);

  // Need to wait for threads to complete...
  for(iter = threads.begin(); iter != threads.end(); ++iter)
  {
    iter->stop();
  }
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
