/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     PuzzleCOnfigProcessor.cpp
 * @brief    Implements a puzzle configuration processing functor.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes  ================================================================

#include "PuzzleConfigProcessor.h"

#include <cstdlib>
#include <unistd.h>
#include <iostream> // Debug

#include "EdgeTabuMap.h"
#include "OutWriter.h"


//=== Constructors/Destructor ==================================================

/**
 * @name   PuzzleConfigProcessor
 * @brief  Default construction of the PuzzleConfigProcessor object.
 * @param  None
 * @return PuzzleConfigProcessor : Constructed instance of PuzzleConfigProcessor
 * @throws None
 */
PuzzleConfigProcessor::PuzzleConfigProcessor() :
  processingFlag(false),
  runningFlag(true),
  tiles(NULL)
{
  // Nothing to do here
}

//------------------------------------------------------------------------------

/**
 * @name   ~PuzzleConfigProcessor
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
PuzzleConfigProcessor::~PuzzleConfigProcessor()
{
  // Maybe do something here?
}


//=== Public Methods ===========================================================

/**
 * @name   run
 * @brief  Function intended to be used as a thread
 * @param  None
 * @return void
 * @throws None
 */
void
PuzzleConfigProcessor::run(PuzzleConfigProcessor* self)
{
  while(self->runningFlag)
  {
    if(self->processingFlag)
    {
      self->assessSolution();
      self->processingFlag = false;
    }
    else
    {
      usleep(1); // Take a short nap to not fry the CPU
    }
  }
}

//------------------------------------------------------------------------------

/**
 * @name   start
 * @brief  Generates and launches the thread
 * @param  None
 * @return void
 * @throws None
 */
void
PuzzleConfigProcessor::start()
{
  runningFlag = true;
  processingFlag = false;
  thisThread = std::thread(run, this);
}

//------------------------------------------------------------------------------

/**
 * @name   stop
 * @brief  Stops the running thread after current processing is complete
 * @param  None
 * @return void
 * @throws None
 */
void
PuzzleConfigProcessor::stop()
{
  runningFlag = false;
  thisThread.join();
}


//=== Private Methods ==========================================================

/**
 * @name   assessSolution
 * @brief  Assesses if the puzzle solution is valid; writes to file if it
 *         is.
 * @note   If this method is being executed, the Taskmaster has previously
 *         supplied a unique puzzle configuration, and thus the current
 *         configuration does not need to be cross-referenced against its
 *         tabu map.
 * @param  None
 * @return void
 * @throws None
 */
void
PuzzleConfigProcessor::assessSolution()
{
  bool match = true;
  std::uint8_t* e1 = nullptr;
  std::uint8_t* e2 = nullptr;
  Tile t00 = tiles[currentConfig.locations[0][0]];
  Tile t01 = tiles[currentConfig.locations[0][1]];
  Tile t02 = tiles[currentConfig.locations[0][2]];
  Tile t10 = tiles[currentConfig.locations[1][0]];
  Tile t11 = tiles[currentConfig.locations[1][1]];
  Tile t12 = tiles[currentConfig.locations[1][2]];
  Tile t20 = tiles[currentConfig.locations[2][0]];
  Tile t21 = tiles[currentConfig.locations[2][1]];
  Tile t22 = tiles[currentConfig.locations[2][2]];

  // Assess edge 00R-01L
  e1 = t00.getEdge(currentConfig.orientations[0][0], TileSide::eRight);
  e2 = t01.getEdge(currentConfig.orientations[0][1], TileSide::eLeft);
  match = matchEdges(e1,e2);

  // Assess edge 01R-02L
  if(match)
  {
    e1 = t01.getEdge(currentConfig.orientations[0][1], TileSide::eRight);
    e2 = t02.getEdge(currentConfig.orientations[0][2], TileSide::eLeft);
    match = matchEdges(e1,e2);
  }

  // Assess edge 10R-11L
  if(match)
  {
    e1 = t10.getEdge(currentConfig.orientations[1][0], TileSide::eRight);
    e2 = t11.getEdge(currentConfig.orientations[1][1], TileSide::eLeft);
    match = matchEdges(e1,e2);
  }

  // Assess edge 11R-12L
  if(match)
  {
    e1 = t11.getEdge(currentConfig.orientations[1][1], TileSide::eRight);
    e2 = t12.getEdge(currentConfig.orientations[1][2], TileSide::eLeft);
    match = matchEdges(e1,e2);
  }

  // Assess edge 20R-21L
  if(match)
  {
    e1 = t20.getEdge(currentConfig.orientations[2][0], TileSide::eRight);
    e2 = t21.getEdge(currentConfig.orientations[2][1], TileSide::eLeft);
    match = matchEdges(e1,e2);
  }

  // Assess edge 21R-22L
  if(match)
  {
    e1 = t21.getEdge(currentConfig.orientations[2][1], TileSide::eRight);
    e2 = t22.getEdge(currentConfig.orientations[2][2], TileSide::eLeft);
    match = matchEdges(e1,e2);
  }

  // Assess edge 00B-10T
  if(match)
  {
    e1 = t00.getEdge(currentConfig.orientations[0][0], TileSide::eBottom);
    e2 = t10.getEdge(currentConfig.orientations[1][0], TileSide::eTop);
    match = matchEdges(e2,e1);
  }

  // Assess edge 10B-20T
  if(match)
  {
    e1 = t10.getEdge(currentConfig.orientations[1][0], TileSide::eBottom);
    e2 = t20.getEdge(currentConfig.orientations[2][0], TileSide::eTop);
    match = matchEdges(e2,e1);
  }

  // Assess edge 01B-11T
  if(match)
  {
    e1 = t01.getEdge(currentConfig.orientations[0][1], TileSide::eBottom);
    e2 = t11.getEdge(currentConfig.orientations[1][1], TileSide::eTop);
    match = matchEdges(e2,e1);
  }

  // Assess edge 11B-21T
  if(match)
  {
    e1 = t11.getEdge(currentConfig.orientations[1][1], TileSide::eBottom);
    e2 = t21.getEdge(currentConfig.orientations[2][1], TileSide::eTop);
    match = matchEdges(e2,e1);
  }

  // Assess edge 02B-12T
  if(match)
  {
    e1 = t02.getEdge(currentConfig.orientations[0][2], TileSide::eBottom);
    e2 = t12.getEdge(currentConfig.orientations[1][2], TileSide::eTop);
    match = matchEdges(e2,e1);
  }
  // Assess edge 12B-22T
  if(match)
  {
    e1 = t12.getEdge(currentConfig.orientations[1][2], TileSide::eBottom);
    e2 = t22.getEdge(currentConfig.orientations[2][2], TileSide::eTop);
    match = matchEdges(e2,e1);
  }

  // Do we have a winner??
  if(match)
  {
    // YES WE DO!!
    OutWriter& reporter = OutWriter::getInstance();
    reporter.writeSolution(currentConfig);
    std::cout << "STATUS: Found a winner!" << std::endl;
  }
}

//------------------------------------------------------------------------------

/**
 * @name   matchEdges
 * @brief  Assesses if two tile edges match; returns TRUE if they do.
 *         Note: Assumes edge1 is in proper order and edge2 is in reverse order.
 * @param  edge1 (std::uint8_t*) - edge map of a side of a tile
 * @param  edge2 (std::uint8_t*) - edge map of a side of another tile
 * @return bool : returns TRUE if edge pairing matches
 * @throws None
 */
bool
PuzzleConfigProcessor::matchEdges(std::uint8_t* edge1, std::uint8_t* edge2)
{
  EdgePair edges;
    edges.first  = edge1;
    edges.second = edge2;

  // First check if previously analyzed
  EdgeTabuMap& etm = EdgeTabuMap::getInstance();
  std::uint8_t result = etm.checkEdgePair(edges);
  if(result != 0)
  {
    return (result == 1);
  }
  else
  {
    // Gotta match the edges up
    std::uint32_t eLen = tiles[0].getLength();
    bool match = true;

    // Check e1[0] with e2[-2..-1] if it's an edge point
    if(edge1[0])
    {
      match = ((edge2[eLen-1] == 1) ||
               (edge2[eLen-2] == 1));
    }

    // Check e1[-1] with e2[0..1] if it's an edge point
    if(edge1[eLen-1])
    {
      match = match && ((edge2[0] == 1) ||
                        (edge2[1] == 1));
    }

    // Check the rest of the edge points
    for(std::uint32_t i = 1; i < eLen-2 && match; ++i)
    {
      if(edge1[i])
      {
        match = match && ((edge2[eLen - i] == 1)     ||
                          (edge2[eLen - 1 - i] == 1) ||
                          (edge2[eLen - 2 - i] == 1));
      }
    }

    // Check the final results
    if(match)
    {
      etm.setEdgePair(edges, 1);
    }
    else
    {
      etm.setEdgePair(edges, -1);
    }
    return match;
  }
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
