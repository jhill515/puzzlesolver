/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     EdgeTabuMap.h
 * @brief    Describes a tabu search memoization strategy for reducing
 *           computational burden of edge comparisons.
 *
 * @author   John T Hill IV(jhill515@gmail.com)
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef EDGETABUMAP_H_
#define EDGETABUMAP_H_

//=== Includes  ================================================================

#include <map>
#include <cstdint>
#include <utility>


//=== Type Definitions =========================================================

/**
 * @name  EdgePair
 * @base  std::pair<std::uint8_t*, std::uint8_t*>
 * @brief Simplifies references of edge pairs
 */
typedef std::pair<std::uint8_t*, std::uint8_t*> EdgePair;

//==============================================================================

/**
 * @class   EdgeTabuMap
 * @extends None
 * @brief   See file brief
 */
class EdgeTabuMap
{
  public: /// @publicsection
    //--- Destructor -----------------------------------------------------------

    /**
     * @name   ~EdgeTabuMap
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~EdgeTabuMap();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   getInstance
     * @brief  Returns an instance of the EdgeTabuMap
     * @param  None
     * @return EdgeTabuMap& : Instance of the singleton
     * @throws None
     */
    static
    EdgeTabuMap&
    getInstance();

    /**
     * @name   checkEdgePair
     * @brief  Returns edge matching if available, otherwise that it has never
     *         been analyzed.
     * @param  pair (EdgePair) - Requested edge pairing
     * @return std::uint8_t : Returns following convention of tabuMap;
     * @throws None
     */
    std::uint8_t
    checkEdgePair(EdgePair pair);

    /**
     * @name   setEdgePair
     * @brief  Sets the value of the edge pair analysis
     * @param  pair (EdgePair) - Analyzed edge pairing
     * @param  result (std::uint8_t) - Values expected to follow convention of
     *         tabuMap
     * @return void
     * @throws None
     */
    void
    setEdgePair(EdgePair pair, std::uint8_t result);


  private: /// @privatesection
    //--- Private Definitions --------------------------------------------------

    /**
     * @name  EdgePairMap
     * @base  std::map<EdgePair, std::uint8_t>
     * @brief Simplifies references of the edge pairing map
     */
    typedef std::map<EdgePair, std::uint8_t> EdgePairMap;

    /**
     * @name  EdgePairMapIter
     * @base  EdgePairMap::iterator
     * @brief Simplifies references to the EdgePairMap iterator
     */
    typedef EdgePairMap::iterator EdgePairMapIter;


    //--- Private Members ------------------------------------------------------

    /**
     * @name   tabuMap
     * @brief  The map of edge pairs analyzed and their results
     * @units  Values are marked as follows:
     *         -1 : Does not match
     *          0 : Never visited
     *         +1 : Matches
     * @bounds N/A
     */
    EdgePairMap tabuMap;


    //--- Constructor ----------------------------------------------------------

    /**
     * @name   EdgeTabuMap
     * @brief  Default construction of the EdgeTabuMap object.
     * @param  None
     * @return EdgeTabuMap : Constructed instance of EdgeTabuMap
     * @throws None
     */
    EdgeTabuMap();
};


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
