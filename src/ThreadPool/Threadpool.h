/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Threadpool.h
 * @brief    Declares a manager for functor threads
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_

//=== Includes  ================================================================

#include <vector>

#include "PuzzleConfigProcessor.h"
#include "Tile.h"


//=== Type Definitions =========================================================

/**
 * @class   Threadpool
 * @extends None
 * @brief   Manages functor threads
 */
class Threadpool
{
  public: /// @publicsection
    //--- Constructors/Destructor ----------------------------------------------

    /**
     * @name   Threadpool
     * @brief  Default construction of the Threadpool object.
     * @param  None
     * @return Threadpool : Constructed instance of Threadpool
     * @throws None
     */
    Threadpool();

    /**
     * @name   Threadpool
     * @brief  Default construction of the Threadpool object.
     * @param  None
     * @return Threadpool : Constructed instance of Threadpool
     * @throws None
     */
    Threadpool(const std::uint32_t& threadCount);

    /**
     * @name   ~Threadpool
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~Threadpool();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   setTiles
     * @brief  Sets the tiles structure for each thread
     * @param  puzzlePieces (Tile*) - Reference to a 9-element array of tiles
     * @return void
     * @throws None
     */
    void
    setTiles(Tile* puzzlePieces);

    /**
     * @name   launchThreads
     * @brief  Launches all threads, monitors for task completion, and
     *         relaunches if there are more tasks to accomplish.
     * @param  arg_name : arg_brief
     * @return return_type : ret_brief
     * @throws except_type : except_brief
     */
    void
    launchThreads();

    /**
     * @name   monitorThreads
     * @brief  Monitors threads and provides them with tasks if available
     * @param  None
     * @return void
     * @throws None
     */
    void
    monitorThreads();


  private: /// @privatesection
    //--- Private Members ------------------------------------------------------

    /**
     * @name   threads
     * @brief  Container of functors
     * @units  N/A
     * @bounds N/A
     */
    std::vector<PuzzleConfigProcessor> threads;

};


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
