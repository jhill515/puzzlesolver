/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     EdgeTabuMap.cpp
 * @brief    Implements a tabu search memoization strategy for reducing
 *           computational burden of edge comparisons.
 *
 * @author   John T Hill IV(jhill515@gmail.com)
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "EdgeTabuMap.h"

//=== Constructors/Destructor ==================================================

/**
 * @name   EdgeTabuMap
 * @brief  Default construction of the EdgeTabuMap object.
 * @param  None
 * @return EdgeTabuMap : Constructed instance of EdgeTabuMap
 * @throws None
 */
EdgeTabuMap::EdgeTabuMap()
{
  // TODO: Insert code here
}

//------------------------------------------------------------------------------

/**
 * @name   ~EdgeTabuMap
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
EdgeTabuMap::~EdgeTabuMap()
{
  // TODO: Insert code here
}


//=== Public Methods ===========================================================

/**
 * @name   getInstance
 * @brief  Returns an instance of the EdgeTabuMap
 * @param  None
 * @return EdgeTabuMap& : Instance of the singleton
 * @throws None
 */
EdgeTabuMap&
EdgeTabuMap::getInstance()
{
  static EdgeTabuMap instance;
  return instance;
}

//------------------------------------------------------------------------------

/**
 * @name   checkEdgePair
 * @brief  Returns edge matching if available, otherwise that it has never
 *         been analyzed.
 * @param  pair (EdgePair) - Requested edge pairing
 * @return std::uint8_t : Returns following convention of tabuMap;
 * @throws None
 */
std::uint8_t
EdgeTabuMap::checkEdgePair(EdgePair pair)
{
  std::uint8_t ret = 0;
  EdgePairMapIter ptr = tabuMap.find(pair);
  if(ptr == tabuMap.end())
  {
    EdgePair rev;
    rev.first  = pair.second;
    rev.second = pair.first;
    ptr = tabuMap.find(rev);
  }
  if(ptr == tabuMap.end())
  {
    tabuMap[pair] = 0;
  }
  else
  {
    ret = ptr->second;
  }
  return ret;
}

//------------------------------------------------------------------------------

/**
 * @name   setEdgePair
 * @brief  Sets the value of the edge pair analysis
 * @param  pair (EdgePair) - Analyzed edge pairing
 * @param  result (std::uint8_t) - Values expected to follow convention of
 *         tabuMap
 * @return void
 * @throws None
 */
void
EdgeTabuMap::setEdgePair(EdgePair pair, std::uint8_t result)
{
  tabuMap[pair] = result;
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
