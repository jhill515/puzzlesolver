/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     PuzzleConfigProcessor.h
 * @brief    Declares a puzzle configuration processing functor.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef PUZZLECONFIGPROCESSOR_H_
#define PUZZLECONFIGPROCESSOR_H_

//=== Includes  ================================================================

#include <thread>
#include <cstdint>

#include "PuzzleConfig.h"
#include "Tile.h"


//=== Type Definitions =========================================================

/**
 * @class   PuzzleConfigProcessor
 * @extends None
 * @brief   Given a puzzle configuration, determines if it is a potential
 *          solution; records to file if so.
 */
class PuzzleConfigProcessor
{
  public: /// @publicsection
    //--- Constructors/Destructor ----------------------------------------------

    /**
     * @name   PuzzleConfigProcessor
     * @brief  Default construction of the PuzzleConfigProcessor object.
     * @param  None
     * @return PuzzleConfigProcessor : Constructed instance of PuzzleConfigProcessor
     * @throws None
     */
    PuzzleConfigProcessor();

    /**
     * @name   ~PuzzleConfigProcessor
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~PuzzleConfigProcessor();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   setPuzzleConfig
     * @brief  Sets the puzzle configuration for solution assessment
     * @param  config (const PuzzleConfig&) - current puzzle configuration
     * @return void
     * @throws None
     */
    void
    setPuzzleConfig(const PuzzleConfig& config)
    { currentConfig = config; processingFlag = true; }

    /**
     * @name   setTiles
     * @brief  Sets the reference to the array of puzzle tiles
     * @param  puzzlePieces (Tile*) - Reference to a 9-element array of tiles
     * @return void
     * @throws None
     */
    void
    setTiles(Tile* puzzlePieces)
    { tiles = puzzlePieces; }

    /**
     * @name   run
     * @brief  Function intended to be used as a thread
     * @param  None
     * @return void
     * @throws None
     */
    static
    void
    run(PuzzleConfigProcessor* self);

    /**
     * @name   start
     * @brief  Generates and launches the thread
     * @param  None
     * @return void
     * @throws None
     */
    void
    start();

    /**
     * @name   stop
     * @brief  Stops the running thread after current processing is complete
     * @param  None
     * @return void
     * @throws None
     */
    void
    stop();

    /**
     * @name   isProcessing
     * @brief  Returns state of the processingFlag
     * @param  None :
     * @return bool : See processingFlag
     * @throws None :
     */
    bool
    isProcessing() { return processingFlag; }


  private: /// @privatesection
    //--- Private Members ------------------------------------------------------

    /**
     * @name   currentConfig
     * @brief  Current puzzle configuration to assess
     * @units  N/A
     * @bounds N/A
     */
    PuzzleConfig currentConfig;

    /**
     * @name   processingFlag
     * @brief  Flag to indicate if the puzzle configuration is currently
     *         processing
     * @units  N/A
     * @bounds N/A
     */
    bool processingFlag;

    /**
     * @name   runningFlag
     * @brief  Flag to control thread completion if it is no longer needed
     * @units  N/A
     * @bounds N/A
     */
    bool runningFlag;

    /**
     * @name   tiles
     * @brief  Array of puzzle tiles (assuming length of 9)
     * @units  N/A
     * @bounds N/A
     */
    Tile* tiles;

    /**
     * @name   thisThread
     * @brief  Running thread structure
     * @units  N/A
     * @bounds N/A
     */
    std::thread thisThread;



    //--- Private Methods ------------------------------------------------------

    /**
     * @name   assessSolution
     * @brief  Assesses if the puzzle solution is valid; writes to file if it
     *         is.
     * @note   If this method is being executed, the Taskmaster has previously
     *         supplied a unique puzzle configuration, and thus the current
     *         configuration does not need to be cross-referenced against its
     *         tabu map.
     * @param  None
     * @return void
     * @throws None
     */
    void
    assessSolution();

    /**
     * @name   matchEdges
     * @brief  Assesses if two tile edges match; returns TRUE if they do
     * @param  edge1 (std::uint8_t*) - edge map of a side of a tile
     * @param  edge2 (std::uint8_t*) - edge map of a side of another tile
     * @return bool : returns TRUE if edge pairing matches
     * @throws None
     */
    bool
    matchEdges(std::uint8_t* edge1, std::uint8_t* edge2);

};

#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
