/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV>; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Argstruct.h
 * @brief    Defines a struct to contain arguments passed from command line.
 *
 * @author   John T Hill IV(jhill515@gmail.com)
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef ARGSTRUCT_H_
#define ARGSTRUCT_H_

//=== Includes =================================================================

#include <cstdint>
#include <string>


//=== Type Definition ==========================================================

/**
 * @struct ArgStruct
 * @brief  Struct to contain arguments passed from command line
 */
typedef struct ArgStruct_t
{
  /**
   * @name   numThreads
   * @brief  Number of threads allowed in the thread pool
   * @units  N/A
   * @bounds N/A
   */
  std::uint32_t numThreads;

  /**
   * @name   imgFilenames
   * @brief  Array containing the filenames to be used as puzzle pieces
   * @units  N/A
   * @bounds N/A
   */
  std::string imgFilenames[9];
} ArgStruct;


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill iV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
