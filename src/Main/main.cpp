/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV>; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     main.cpp
 * @brief    Main execution file
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 8, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes =================================================================

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <opencv2/opencv.hpp>

#include "Image.h"
#include "Threadpool.h"
#include "OutWriter.h"

#include "ArgStruct.h"


//=== Function Declarations ====================================================

/**
 * @name   helpdoc
 * @brief  Outputs intended usage information
 * @param  None
 * @return void
 * @throws None
 */
void
helpdoc();

/**
 * @name   parseArgs
 * @brief  Parses command line arguments
 * @param  None
 * @return void
 * @throws None
 */
ArgStruct*
parseArgs(int argc, char** argv);


//=== MAIN =====================================================================

/**
 * @name   main
 * @brief  brief
 * @param  aArgc : Number of arguments on the command line
 * @param  aArgv : Array of char-arrays representing the command line arguments
 * @return int : IF zero, process ended successfully
 * @throws None
 */
int
main(int argc, char** argv)
{
  ArgStruct* args = parseArgs(argc,argv);
  if(!args)
  {
    // Must have had invalid arguments
    return 0;
  }

  // Get and pre-process tile images
  Tile tiles[9];
  for(int i=0; i<9; ++i)
  {
    cv::Mat img = cv::imread(args->imgFilenames[i], CV_LOAD_IMAGE_COLOR);
    Image tileImg(img);
    tiles[i]  = tileImg.generateTile();
  }

  // DEBUG - start
  std:: cout << "Starting the worker threads!" << std::endl;
  // DEBUG - end

  Threadpool workers(args->numThreads);
  workers.setTiles(tiles);
  workers.launchThreads();
  workers.monitorThreads();

  // DEBUG - start
  std:: cout << "Worker threads finished!" << std::endl;
  // DEBUG - end


  OutWriter& output = OutWriter::getInstance();
  output.closeFile();

  return 0;
}


//=== Function Implementations =================================================

/**
 * @name   helpdoc
 * @brief  Outputs intended usage information
 * @param  None
 * @return void
 * @throws None
 */
void
helpdoc()
{
  std::cout << "\n"
            << "puzzlesolver\n"
            << "-------------------------------------------------------------------------------\n"
            << "Usage: puzzlesolver [num_threads] [Img0] [Img1] [Img2] [Img3] [Img4] [Img5]    \n"
            << "        [Img6] [Img7] [Img8]                                                   \n"
            << "Synopsis: Solves 3x3 tiled puzzles described in bitmap images (RGB, YUV, or    \n"
            << "CkCrY). It is assumed that these images are represented as 24-bit pixels       \n"
            << "(8-bits per channel). Pieces in the images may be of arbitrary cardinal        \n"
            << "orientation. The provided solution is a text file describing all possible      \n"
            << "puzzle solutions. These solutions will appear in the following format:         \n"
            << "\n"
            << "Pieces   Orientations\n"
            << "---------------------\n"
            << "0 1 2    0 1 2\n"
            << "3 4 5    3 0 1\n"
            << "6 7 8    2 3 0\n"
            << "\n"
            << "0 2 1    3 0 2\n"
            << "7 4 5    0 0 2\n"
            << "8 3 6    2 2 0\n"
            << "...\n"
            << "\n"
            << "Puzzle pieces are identified in order passed as command line arguments from    \n"
            << "zero to nine (See above example first solution).\n"
            << "\n"
            << "Orientations are identified by cardinal directions:\n"
            << "0 : Original orientation\n"
            << "1 : Rotated 90deg clockwise\n"
            << "2 : Rotated 180deg\n"
            << "3 : Rotated 90deg counter-clockwise\n"
            << std::endl;
  return;
}

//------------------------------------------------------------------------------

/**
 * @name   parseArgs
 * @brief  Parses command line arguments
 * @param  None
 * @return void
 * @throws None
 */
ArgStruct*
parseArgs(int argc, char** argv)
{
  ArgStruct* myargs = new ArgStruct();
  if(argc < 10)
  {
    helpdoc();
    delete myargs;
    return NULL;
  }

  // Parse first arg
  int num = std::atoi(argv[1]);
  if(num < 1)
  {
    helpdoc();
    delete myargs;
    return NULL;
  }
  else
  {
    myargs->numThreads = (std::uint32_t)num;
  }

  // Parse remaining args
  for(int i=2; i<10; ++i)
  {
    myargs->imgFilenames[i-2] = argv[i];
    // FIXME: Add some defensive coding here; what if it isn't a *.bmp?
  }

  return myargs;
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
