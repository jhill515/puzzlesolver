/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Taskmaster.h
 * @brief    Declaration of a solution generator class.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

#ifndef TASKMASTER_H_
#define TASKMASTER_H_

//=== Includes  ================================================================

#include <cstdint>
#include <map>

#include "PuzzleConfig.h"


//=== Type Definitions =========================================================

/**
 * @class   Taskmaster
 * @extends None
 * @brief   Generates candidate solutions if any need to be investigated
 */
class Taskmaster
{
  public: /// @publicsection
    //--- Destructor -----------------------------------------------------------

    /**
     * @name   ~Taskmaster
     * @brief  A simple destructor.
     * @param  None
     * @return None
     * @throws None
     */
    virtual
    ~Taskmaster();


    //--- Public Methods -------------------------------------------------------

    /**
     * @name   getInstance
     * @brief  Returns instance of the singleton
     * @param  None
     * @return Taskmaster& : Instance of the singleton
     * @throws None
     */
    static
    Taskmaster&
    getInstance();

    /**
     * @name   hasMoreWork
     * @brief  Returns TRUE if a new task is available
     * @param  None
     * @return bool : See brief
     * @throws None
     */
    bool
    hasMoreWork()
    { return moreWork; }

    /**
     * @name   getWork
     * @brief  Gets a copy of the next puzzle configuration to analyze
     * @param  None
     * @return PuzzleConfig : To be analyzed
     * @throws None
     */
    PuzzleConfig
    getWork();


  private: /// @privatesection
    //--- Private Data Definitions ---------------------------------------------

    typedef std::map<PuzzleConfig, std::int8_t, PuzzleConfigCompare>::iterator
            tabuConfigIter;


    //--- Private Members ------------------------------------------------------

    /**
     * @name   currentConfig
     * @brief  Describes the next available puzzle configuration to solve
     * @units  N/A
     * @bounds N/A
     */
    PuzzleConfig currentConfig;

    /**
     * @name   placementPerm
     * @brief  An array describing tile placement permutations; needs to be
     *         persistent separately from currentConfig
     * @units  N/A
     * @bounds N/A
     */
    std::uint8_t placementPerm[9];

    /**
     * @name   moreWork
     * @brief  A flag indicating if there is more work to be done.
     * @units  N/A
     * @bounds N/A
     */
    bool moreWork;

    /**
     * @name   tabuConfigs
     * @brief  A map of unique puzzle configurations analyzed
     * @units  N/A
     * @bounds N/A
     */
    std::map<PuzzleConfig, std::int8_t, PuzzleConfigCompare> tabuConfigs; // FIXME

    //--- Constructor ----------------------------------------------------------

    /**
     * @name   Taskmaster
     * @brief  Default construction of the Taskmaster object.
     * @param  None
     * @return Taskmaster : Constructed instance of Taskmaster
     * @throws None
     */
    Taskmaster();


    //--- Private Methods ------------------------------------------------------

    /**
     * @name   setNextOrientationCombo
     * @brief  Changes the combination of the orientations in the current
     * @param  None
     * @return void
     * @throws None
     */
    void
    setNextOrientationCombo();

    /**
     * @name   setNextConfig
     * @brief  Updates the puzzle configuration to the next available
     *         unique configuration to analyze
     * @param  None :
     * @return void :
     * @throws None :
     */
    void
    setNextConfig();

};


#endif

/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
