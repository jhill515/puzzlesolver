/*******************************************************************************
 * @copyright Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * @project  puzzlesolver
 * @file     Taskmaster.cpp
 * @brief    Implementation of a solution generator class.
 *
 * @author   John T Hill IV(jhill515@gmail.com
 * @date     Jul 9, 2017
 *
 * @attention Microsoft Reference Source License (Ms-RSL)
 * @attention See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */

//=== Includes ================================================================

#include "Taskmaster.h"

#include <algorithm>


//=== Constructors/Destructor ==================================================

/**
 * @name   Taskmaster
 * @brief  Default construction of the Taskmaster object.
 * @param  None
 * @return Taskmaster : Constructed instance of Taskmaster
 * @throws None
 */
Taskmaster::Taskmaster() :
  moreWork(true)
{
  placementPerm[0] = 0;
  placementPerm[1] = 1;
  placementPerm[2] = 2;
  placementPerm[3] = 3;
  placementPerm[4] = 4;
  placementPerm[5] = 5;
  placementPerm[6] = 6;
  placementPerm[7] = 7;
  placementPerm[8] = 8;

  for(int i=0; i<3; ++i)
  {
    for(int j=0; j<3; ++j)
    {
      currentConfig.orientations[i][j] = Orientation::eOrient0;
      currentConfig.locations[i][j] = 3*i + j;
    }
  }
}

//------------------------------------------------------------------------------

/**
 * @name   ~Taskmaster
 * @brief  A simple destructor.
 * @param  None
 * @return None
 * @throws None
 */
Taskmaster::~Taskmaster()
{
  // Nothing to do here
}


//=== Public Methods ===========================================================

/**
 * @name   getInstance
 * @brief  Returns instance of the singleton
 * @param  None
 * @return Taskmaster& : Instance of the singleton
 * @throws None
 */
Taskmaster&
Taskmaster::getInstance()
{
  static Taskmaster instance;
  return instance;
}

//------------------------------------------------------------------------------

/**
 * @name   getWork
 * @brief  Gets a copy of the next puzzle configuration to analyze
 * @param  None
 * @return PuzzleConfig : To be analyzed
 * @throws None
 */
PuzzleConfig
Taskmaster::getWork()
{
  PuzzleConfig ret = currentConfig;

  // Set next valid configuration
  do
  {
    // Keep trying until we get the next unique configuration
    setNextConfig();
  }while(tabuConfigs.find(currentConfig) != tabuConfigs.end());
  tabuConfigs[currentConfig] = 0;

  return ret;
}


//=== Private Methods ==========================================================

/**
 * @name   setNextOrientationCombo
 * @brief  Changes the combination of the orientations in the current
 * @param  None
 * @return void
 * @throws None
 */
void
Taskmaster::setNextOrientationCombo()
{
  bool bFlag = true;
  for(int i=0; i<3; ++i)
  {
    for(int j=0; j<3; ++j)
    {
      bFlag = true;
      switch(currentConfig.orientations[i][j])
      {
        case Orientation::eOrient0:
        {
          currentConfig.orientations[i][j] = Orientation::eOrient1;
          break;
        }
        case Orientation::eOrient1:
        {
          currentConfig.orientations[i][j] = Orientation::eOrient2;
          break;
        }
        case Orientation::eOrient2:
        {
          currentConfig.orientations[i][j] = Orientation::eOrient3;
          break;
        }
        case Orientation::eOrient3:
        default:
        {
          currentConfig.orientations[i][j] = Orientation::eOrient0;
          bFlag = false;
          break;
        }
      }
      if(bFlag)
      {
        break;
      }
    }
    if(bFlag)
    {
      break;
    }
  }
}

//------------------------------------------------------------------------------

/**
 * @name   setNextConfig
 * @brief  Updates the puzzle configuration to the next available
 *         unique configuration to analyze
 * @param  None :
 * @return void :
 * @throws None :
 */
void
Taskmaster::setNextConfig()
{
  setNextOrientationCombo();
  bool initFlag = true;
  for(int i=0; i<3; ++i)
  {
    for(int j=0; j<3; ++j)
    {
      initFlag =
        initFlag && (currentConfig.orientations[i][j] == Orientation::eOrient0);
    }
  }

  if(initFlag)
  {
    // We need another placement permutation
    std::next_permutation(placementPerm, placementPerm+9);
    for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
      {
        int idx = 3*i + j;
        currentConfig.locations[i][j] = placementPerm[idx];
      }
    }
  }
}


/*******************************************************************************
 * Copyright (c)2017 John T Hill IV; All rights reserved.
 *
 * Microsoft Reference Source License (Ms-RSL)
 * See Ms-RSL.md or request from John T Hill IV for license details.
 *******************************************************************************
 */
