[comment]: # ( @copyright Copyright (c)2017 John T Hill IV; All rights reserved.                                       )
[comment]: # (                                                                                                         )
[comment]: # ( @project Puzzle Solver                                                                                  )
[comment]: # ( @file    Readme.md                                                                                      )
[comment]: # ( @brief   Project readme file                                                                            )
[comment]: # (                                                                                                         )
[comment]: # ( @author  John T Hill IV(jhill515@gmail.com)                                                             )
[comment]: # ( @date    8-Jul-2017                                                                                     )
[comment]: # (                                                                                                         )
[comment]: # ( @attention Microsoft Reference Source License (Ms-RSL)                                                  )
[comment]: # ( @attention See Ms-RSL.md or request from John T Hill IV for license details.                            )



Introduction
========================================================================================================================
This software provides functionality to solve 3x3 tiled puzzles described in bitmap images (RGB, YUV, or CkCrY). It is assumed that these images are represented as 24-bit pixels (8-bits per channel). Pieces in the images may be of arbitrary cardinal orientation.

The provided solution is a text file describing all possible *unique* puzzle solutions (that is, solutions which are not merely rotations of one another). These solutions will appear in the following format:

```
Pieces   Orientations
---------------------
0 1 2    0 1 2
3 4 5    3 0 1
6 7 8    2 3 0

0 2 1    3 0 2
7 4 5    0 0 2
8 3 6    2 2 0

...
```

Puzzle pieces are identified in order passed as command line arguments from zero to eight (See above example first solution).

To understand the interpretation of the *Orientations*, please refer to the figure below:

![Orientation Map](docs/images/OrientationMap.png)

**Current Version:** Kaarta Release (2.0.0) : 15-Oct-2017


Features
------------------------------------------------------------------------------------------------------------------------

* Solves 3x3 puzzles

* Uses 9x9 pixel edge detection to correlate edges of each piece

* Maximum thread pool population can be specified
  * Note, beware of stressing the computing system this process executes upon


Known Issues
------------------------------------------------------------------------------------------------------------------------
* Currently assumes correlation of edge pixels must be adjacent to one another. This means that if the edge slope is greater than +/-2, correlation will fail and a potential solution will be dropped.

* Assumes all puzzle images are the same resolution, which is equal to the resolution of the zeroth image passed (see **Execution Instructions** section).



Build & Installation Instructions
========================================================================================================================
This project uses the Cmake build system v3.5.1 maintained & supported by Kitware. It is available through *Aptitude* on Debian Linux systems (otherwise, go to Kitware's [website](kitware.com/cmake)).


Dependencies
------------------------------------------------------------------------------------------------------------------------
* This software uses OpenCV3 core and imgcodecs


Default Build & Installation Process
------------------------------------------------------------------------------------------------------------------------
Building utilizes the CMake build system and was developed following the ISO C++ 2011 standard. Execute the following commands in a Bash terminal:
```bash
cd $PROJECT_DIR # Assuming PROJECT_DIR is where puzzlesolver project is located
mkdir build && cd build
cmake ../src
make -j
make install
```


Cross-Platform Build Support
------------------------------------------------------------------------------------------------------------------------
Not Applicable.


Testing Support
------------------------------------------------------------------------------------------------------------------------
Not Applicable



Execution Instructions
========================================================================================================================
Execution is accomplished with a command-line call in Bash:
```bash
cd $PROJECT_DIR # Assuming PROJECT_DIR is where puzzlesolver project is located
cd bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/../lib
./puzzlesolver [num_threads] [Img0] [Img1] [Img2] [Img3] [Img4] [Img5] [Img6] [Img7] [Img8]
```
Output will be found in file `puzzlesolver_results.txt`



[comment]: # ( Copyright (c)2017 John T Hill IV; All rights reserved.                                                  )
[comment]: # (                                                                                                         )
[comment]: # ( Microsoft Reference Source License (Ms-RSL)                                                             )
[comment]: # ( See Ms-RSL.md or request from John T Hill IV for license details.                                       )
